import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationPruebaComponent } from './reservation-prueba.component';

describe('ReservationPruebaComponent', () => {
  let component: ReservationPruebaComponent;
  let fixture: ComponentFixture<ReservationPruebaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationPruebaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationPruebaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
