import { Component } from '@angular/core';

@Component({
  selector: 'app-root',//
  templateUrl: './app.component.html',// html associative default , la pagina que se muestra
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'DV-reservations';
}
