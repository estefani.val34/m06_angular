import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reservation-entry',
  templateUrl: './reservation-entry.component.html',
  styleUrls: ['./reservation-entry.component.css']
})
export class ReservationEntryComponent implements OnInit {//es una clase 

  //properties
  propName: String;//lenguaje de tipado estatico, si no es un string me dara un error 

  constructor() { } //contructor crar una clase cada vez que es instancada, se llamma solo cuando se hace un new 

  ngOnInit() { // metodo que llama , iniciliatizador de mas de uno , es como el constructor pero lo puedes llamar
    this.propName="estefani";
  }
  
  reservationEntry(): void {// void no devuelve nada, String si devuleve un string
    //alert("Hola");
    console.log(this.propName);
    
  }

}
